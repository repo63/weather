
export const capitalize = (value, type = 1) => {
    if (typeof value !== "string" || isNaN(type) || type < 1 || type > 3)
      return value;
    if (type === 1) return value.trim().charAt(0).toUpperCase() + value.slice(1);
    if (type === 2)
      return value
        .trim()
        .split(" ")
        .map((w) => w.charAt(0).toUpperCase() + w.slice(1).toLowerCase())
        .join(" ");
    if (type === 3) return value.trim().toUpperCase();
  };

export const ConvertMilesToKm = (miles) => {
    const mile = 1.60934;
    const km = mile * miles;
    return km
}

