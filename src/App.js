import React from 'react';
import { Routes, Route } from 'react-router-dom'

import Home from './views/home'
import Info from './views/home/info'

const App = () => {
  return (
    <Routes>
      <Route path='/' element={<Home />}/>
      <Route path='/:day' element={<Info />}/>
    </Routes>
  )
}

export default App;