import React, { useState, useEffect } from 'react';
import { Grid, Card, CardContent } from '@mui/material';
import moment from 'moment';

import Header from '../../components/Header'
import Cards from '../../components/Card'
import CardResume from '../../components/CardResume'
import BackdropLoad from '../../components/BackdropLoad'

const Home = () => {

    const [data, setData] = useState([])
    const [weather, setWeather] = useState(null)
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const get = async () => {
            try {
                const Api = process.env.REACT_APP_API_OPENWEATHER
                let res = await fetch(Api, { headers: { Date: new Date() } });
                let { list } = await res.json();
                const data = list.reduce((acc, element) => {
                    let name = element['dt_txt'].slice(0, 10);
                    (acc[name] ? acc[name] : acc[name] = null || []).push(element);
                    return acc
                }, {});
                const result = Object.entries(data).map(([_, values]) => values);
                console.log(result.length )
                setData(result)
                setWeather(result.length > 5 ? result.slice(0, 1)[0][0] : [])
            } catch (err) {
                // console.log(err)
            } finally {
                setIsLoading((prevState) => !prevState);
            }
        }
        get();
    }, [])

    return (
        <Card
            sx={{
                borderRadius: 0,
                minHeight: '100vh',
                background: 'linear-gradient(to bottom, #360033, #0b8793)'
            }}
        >
            <Header title={'Santiago'} subtitle={moment().format('DD/MM/YYYY')} />
            <CardContent>
                <Grid container spacing={3} justifyContent={'center'} >
                    {
                        isLoading ? <BackdropLoad /> :
                            <Grid container spacing={4} justifyContent={'center'} >
                                {
                                    data.length > 5 && (
                                        <Grid item xs={12} sx={{ mb: 3 }} >
                                            <Cards weather={weather} />
                                        </Grid>
                                    )
                                }
                                {
                                    data.slice(0, 5).map((item, i) => (
                                        <Grid item xs={12} md={2.3} key={i} >
                                            <CardResume dayWeather={item} />
                                        </Grid>
                                    ))
                                }
                            </Grid>
                    }
                </Grid>
            </CardContent>
        </Card>
    )
}

export default Home;

