import React from 'react'
import moment from "moment";
import { Card, CardContent, CardHeader, Grid, Box, Typography, IconButton } from "@mui/material";
import ArrowBackIosOutlinedIcon from '@mui/icons-material/ArrowBackIosOutlined';
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
import { ConvertMilesToKm } from '../../utils'

import Header from '../../components/Header'
import ChartLine from '../../components/Charts/line'

const Info = () => {

    const info = useSelector((state) => state.weatherDay);

    const ChartData = Object.entries(info.dayWeather).filter(([key]) => key !== 'date')
        .map(([_, values]) => {
            return {
                label: moment(values.dt_txt).format('HH:mm'),
                'T° Promedio': Math.round((values.main.temp_min + values.main.temp_max) / 2)
            }
        });

    const Wheathers = [
        {
            value: info.Climates[0]
        },
        {
            value: `Min ${Math.round(info.Min)} °C`,
            image: 'temperature-min.png'
        },
        {
            value: `Max ${Math.round(info.Max)} °C`,
            image: 'temperature-max.png'
        },
        {
            value: `Hr ${Math.min(...info.Humidity)}% - ${Math.max(...info.Humidity)}%`,
            image: 'humidity.png'
        },
        {
            value: `${ConvertMilesToKm(Math.min(...info.Wind)).toFixed(1)} - ${ConvertMilesToKm(Math.max(...info.Wind)).toFixed(1)} km/h`,
            image: 'wind.png'
        }
    ];

    return (
        <Card
            sx={{
                borderRadius: 0,
                minHeight: '100vh',
                background: 'linear-gradient(to bottom, #360033, #0b8793)',
            }}
        >
            <IconButton
                size="large"
                component={NavLink}
                to={'/'}
                sx={{ 
                    color: 'white', 
                    background: '#3a6186', 
                    '&:hover': { 
                        background: '#3D648A',
                        transform: 'scale(1.20)'
                    }, 
                    left: 20, 
                    top: 20 
                }} 
            >
                <ArrowBackIosOutlinedIcon />
            </IconButton>
            <Header title={'Santiago'} subtitle={moment().format('DD/MM/YYYY')} />
            <CardContent>
                <Grid container spacing={3} justifyContent="center" >
                    {
                        Wheathers.map((item, i) => (
                            <Grid
                                item xs={12} sm={6} md={2}
                                key={item.key}
                                sx={{ display: 'flex', justifyContent: "center", flexDirection: "column" }}
                            >
                                <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                                    <img
                                        src={`./images/${item.value === 'Clouds' ? 'cloud.png' :
                                            item.value === 'Rain' ? 'rain.png' :
                                                item.value === 'Clear' ? 'sunny.png' : item.image}`}
                                        height={40}
                                        width={40}
                                        alt={'weathers'}
                                    />
                                    <Typography color={'white'}>
                                        {item.value === 'Rain' ? 'Lluvia' :
                                            item.value === 'Clear' ? 'Despejado' :
                                                item.value === 'Clouds' ? 'Nublado' : item.value}
                                    </Typography>
                                </Box>
                            </Grid>
                        ))
                    }
                    <Grid item xs={12} sm={8} md={8} align={'center'} >
                        <Card sx={{ borderRadius: 4, background: '#EAEAEA', marginTop: '5%' }}>
                            <CardHeader
                                title={
                                    <Typography variant={'h6'}>
                                        Temperatura Promedio
                                    </Typography>
                                }
                            />
                            <CardContent sx={(theme) => ({ p: 2, [theme.breakpoints.down("md")]: { p: 1 } })}>
                                <ChartLine data={ChartData} />
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </CardContent>

        </Card>
    )

}

export default Info;