import { createSlice, configureStore } from "@reduxjs/toolkit";

const weatherSlice = createSlice ({
    name: 'weather',
    initialState: {
        weatherDay: null
    },
    reducers: {
        getWeatherDay: (state, action) => {
            state.weatherDay = action.payload
        }
    }
})

const reducer = weatherSlice.reducer;
export const store = configureStore ({ reducer })
export const { getWeatherDay } = weatherSlice.actions;
