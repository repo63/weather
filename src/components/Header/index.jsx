import React from 'react';
import { CardHeader, Typography } from '@mui/material';

const Header = ({ title, subtitle}) => {

    return(
        <CardHeader
            title={
                <Typography variant={'h4'} align={'center'} color={'white'} >
                    {title}
                </Typography>
            }
            subheader={
                <Typography variant={'body2'} align={'center'} color={'white'} >
                    {subtitle}
                </Typography>
            }
        />
    )
}

export default Header;