import React from 'react';
import { Box } from '@mui/material'
import { ResponsiveContainer, LineChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Line } from 'recharts';

const ChartLine = ({ data }) => {
    return (
        <Box sx={{
            width: '80%',
            height: 250,
            backgroundColor: 'transparent',
            alignItems: 'center',
        }}>
            <ResponsiveContainer>
                <LineChart data={data}>
                    <CartesianGrid strokeDasharray={'4'} />
                    <XAxis dataKey="label" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Line
                        type="line"
                        dataKey={'T° Promedio'}
                        stroke="#44A08D"
                        strokeWidth={3}
                        activeDot={{ r: 10 }}
                    />
                </LineChart>
            </ResponsiveContainer>
        </Box >
    )
};

export default ChartLine;
