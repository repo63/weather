import React from 'react';
import { Typography, Box } from '@mui/material';
import { ConvertMilesToKm } from '../../utils';

const Cards = ({ weather }) => {

    const Wheathers = [
        {
            name: 'T° Actual',
            value: `${Math.round(weather.main.temp_max)}`,
            image: 'temperature.png'
        },
        {
            name: 'Viento',
            value: `${Math.round(ConvertMilesToKm(weather.wind.speed))} km/h`,
            image: 'wind.png'
            
        },
        {
            name: 'Humedad',
            value: `${Math.round(weather.main.humidity)} %`,
            image: 'humidity.png'
            
        }
    ]

    return (
        <Box sx={{display: 'flex', justifyContent: "space-evenly", alignItems: 'center'}}>
            {
                Wheathers.map((item, i) => (
                    <Box key={item.name}>
                        <Box sx={{display: 'flex', alignItems: 'center'}}>
                            <img
                                src={`./images/${item.image}`}
                                height={50}
                                width={50}
                                style={{margin: 3}}
                                alt={'weathers'}
                            />
                            <Typography color={'white'}>
                                { item.value}
                            </Typography>
                        </Box>
                    </Box>
                ))
            }
        </Box>
    )
}

export default Cards