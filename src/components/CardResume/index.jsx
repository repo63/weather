import React from 'react';
import moment from "moment";
import { Card, CardHeader, Typography, Divider, Box, CardContent, LinearProgress, CardActions, Button} from '@mui/material'
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { getWeatherDay } from '../../redux/store'
import { capitalize, ConvertMilesToKm } from '../../utils'

const CardResume = ({ dayWeather }) => {

    const navite = useNavigate();
    const dispatch = useDispatch();

    const day = moment(dayWeather[0].dt_txt).format('dddd');
    const Climates = dayWeather.map((values) => values.weather[0].main)
    const Min = Math.min(...Object.entries(dayWeather).map(([_, values]) => values.main.temp_min));
    const Max = Math.max(...Object.entries(dayWeather).map(([_, values]) => values.main.temp_max));
    const Humidity = Object.entries(dayWeather).map(([_, values]) => values.main.humidity)
    const Wind = Object.entries(dayWeather).map(([_, values]) => values.wind.speed)

    const Wheathers = [
        {
            value: Climates[0]
        },
        {
            value: `Min ${Math.round(Min)} °C`,
            image: 'temperature-min.png'
        },
        {
            value: `Max ${Math.round(Max)} °C`,
            image: 'temperature-max.png'
        },
        {
            value: `Hr ${Math.min(...Humidity)}% - ${Math.max(...Humidity)}%`,
            image: 'humidity.png'
        },
        {
            value: `${ConvertMilesToKm(Math.min(...Wind)).toFixed(1)} - ${ConvertMilesToKm(Math.max(...Wind)).toFixed(1)} km/h`,
            image: 'wind.png'
        }
    ];

    const goInfo = () => {
        const data = {
            Climates,
            Min,
            Max,
            Humidity,
            Wind,
            day,
            dayWeather
        }
        dispatch(getWeatherDay(data))
        navite(`/${day}`)
    }

    return (
        <Card
            sx={{
                marginTop: '5%',
                borderRadius: 3,
                background: 'linear-gradient(to bottom, #283048, #859398)',
                opacity: 0.9,
                '&:hover': {
                    transform: 'scale(1.05)',
                },
            }}
        >
            <CardHeader
                title={
                    <Typography variant={'h6'} align={'center'} color={'white'} >
                        {capitalize(day, 1)}
                    </Typography>
                }
                subheader={
                    <Typography variant={'subtitle2'} align={'center'} color={'white'} >
                        {moment(dayWeather[0].dt_txt).format('DD/MM/YYYY')}
                    </Typography>
                }
            />
            <Divider variant={"middle"} color={'white'} />
            <CardContent>
                <Box sx={{ display: 'flex', flexDirection: 'column'}}>
                    {
                        Wheathers.map((item, i) => (
                            <Box
                                key={item.key}
                                sx={{ display: 'flex', justifyContent: 'space-between', margin: 1, alignItems: 'center' }}
                            >
                                <img
                                    src={`./images/${item.value === 'Clouds' ? 'cloud.png' : 
                                                        item.value === 'Rain' ? 'rain.png' :
                                                            item.value === 'Clear' ? 'sunny.png' : item.image}`}
                                    height={40}
                                    width={40}
                                    alt={'weathers'}
                                />
                                <Typography color={'white'}>
                                    {item.value === 'Rain' ? 'Lluvia' :
                                        item.value === 'Clear' ? 'Despejado' :
                                            item.value === 'Clouds' ? 'Nublado' : item.value}
                                </Typography>

                            </Box>
                        ))
                    }
                </Box>
            </CardContent>
            <CardActions>
                <Button
                    fullWidth
                    variant={'contained'}
                    color={'primary'}
                    sx={{ textTransform: "none" }}
                    onClick={() => goInfo()}
                >
                    Mas Info
                </Button>

            </CardActions>
            <LinearProgress
                color={"info"}
                variant={"determinate"}
                value={100}
                sx={{ height: 4 }}
            />
        </Card>
    )
}

export default CardResume;